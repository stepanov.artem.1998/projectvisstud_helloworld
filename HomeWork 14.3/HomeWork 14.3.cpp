﻿

#include <iostream>
#include <string>
#include <iomanip>
#include <stdint.h>


int main()
{
  
    setlocale(LC_ALL, "Russian");

    std::string text = "Привет, Скиллбокс! Это моя домашняя работа по уроку 14.3. Мне понравился урок!)";
    std::cout << text << "\n";
    std::cout << text.length() << "\n";
   
    std::cout << text[0] << "\n";
    std::cout << text.front() << "\n";


    std::cout << text[text.length()-1] << "\n";
    std::cout << text.back() << "\n";

  
    return 0;

}

