﻿
#include <iostream>
#include <string>

// Функция вывода чётных и нечетных чисел 
// При нажатие 1 , чётные числа и при нажатия 0 нечётные;
void OddNumber(int N, bool Event)
{
   
    for (int i = 0; i <= N; ++i)
    {

        if (Event == 1)
        {
            if (i % 2 == 0)
            {
                std::cout << i << ", ";

            }
        }
        else if (Event == 0)
        {
            if (i % 2 != 0)
            {
                int e = i;
                std::cout << e << ", ";
            }
        }


    }
    
}

int main()
{
    int x;
    int y;
    
    
    std::cout << "Enter the number: ";
    std::cin >> x;

    std::cout << "\n" << "Enter 1 (true) or 0 (false): ";
    std::cin >> y;
   

    OddNumber(x, y);

    
}

