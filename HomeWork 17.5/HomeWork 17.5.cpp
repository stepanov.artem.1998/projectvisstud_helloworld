﻿

#include <iostream>

class Vector
{
public:
    Vector()//: x(5), y(5), z(5)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z;
    }
    void VectorModule()
    {
        double a = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
         std::cout << "\n" << "Vector module : " << a <<"\n";
    }
private:
    double x = 5;
    double y = 5;
    double z = 5;

};

class Specifications
{
private:
    int Years = 23;
    double Height = 175.6;
    double Weight = 70.65;

public:
    Specifications()
    {}
    Specifications( int Y, double H, double W): Years(Y), Height(H), Weight(W)
    {}
    
    void Show()
    {
        std::cout<<"Years :"<< Years <<"\n"<<"Height :"<< Height <<"\n"<<"Weight :"<< Weight <<"\n";
    }
};

int main()
{
    Vector v;
    v.Show();
    v.VectorModule();
    Specifications Vladimir;
    Vladimir.Show();

}

