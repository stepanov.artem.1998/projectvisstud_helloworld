﻿
#include <iostream>

int main()
{
	setlocale(LC_ALL, "Russian");

	const int N = 10; // Введите параметр массива

	int array1[N][N]={};

	int n = N - 1;


	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			array1[i][j] = i + j;
			std::cout << array1[i][j] << " ";
		}
		std::cout << "\n";
	}
	
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			array1[i][n] += array1[i][j];//  сложение элементов строки массива
		}
	}
	
	// Сумма строки индекс которой равен остатку деления от Даты на N
	int O;
	std::cout << "\n" << "Введите число месяца :";
	std::cin >> O;

	if (O >= N)
	{
		int T = O % N;

		for (int i = 0; i < n; !i++)
		{
			for (int j = 0; j < n; j++)
			{
				array1[T][n] += array1[n][j];//  сложение элементов строки массива
			}
		}
		std::cout << "Сумма строки " << T << " :" << array1[T][n] << "\n";
	}
	else if (O < N)
	{
		int R = N % O;

		for (int i = 0; i <n; i++)
		{
			for (int j = 0; j <n; j++)
			{
				array1[R][n] += array1[n][j];//  сложение элементов строки массива
			}
		}
		std::cout << "Сумма строки " << R << " :" << array1[R][n] << "\n";
	}
	
}

